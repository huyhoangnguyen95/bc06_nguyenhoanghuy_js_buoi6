// bài 1

function xemKetQua() {
  var total = 0;

  for (var i = 1; total < 10000; i++) {
    total += i;
    console.log(i, total);
    document.getElementById("result1").innerHTML = ` 
    Số cần tìm là : ${i}
    `;
  }
}

// bài 2

function tinhTong() {
  var numX = document.getElementById("txt-numX").value * 1;
  var numN = document.getElementById("txt-numN").value * 1;
  var total = 0;

  for (var i = 1; i <= numN; i++) {
    total += Math.pow(numX, i);
  }
  document.getElementById("result2").innerHTML = ` 
  Tổng của lũy thừa là : ${total}
  `;
}

// bài 3

function tinhGiaiThua() {
  var numN = document.getElementById("txt-numberN").value * 1;
  var result = 1;

  for (var i = 1; i <= numN; i++) {
    result *= i;
  }
  document.getElementById("result3").innerHTML = ` 
  Kết quả giai thừa là : ${result}
  `;
}

// bài 4

function taoTheDiv() {
  var contentHTML = "";

  for (var i = 1; i <= 10; i++) {
    if (i % 2 == 0) {
      contentHTML += ` <p class="alert alert-danger mb-0"> Div chẵn ${i} </p> `;
    } else {
      contentHTML += ` <p class="alert alert-primary mb-0"> Div lẻ ${i} </p> `;
    }
  }
  document.getElementById("result4").innerHTML = contentHTML;
}
